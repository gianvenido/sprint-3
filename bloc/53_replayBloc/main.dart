import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:replay_bloc/replay_bloc.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // const MyApp({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: BlocProvider(
        create: (_) => CounterBloc(),
        child: MainPage(),
      ),
    );
  }
}

class MainPage extends StatelessWidget {
  /// {@macro app}
  // const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => CounterBloc(),
      child: const MaterialApp(
        home: CounterPage(),
      ),
    );
  }
}

class CounterPage extends StatelessWidget {
  /// {@macro counter_page}
  const CounterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Video 93x - ReplayBloc \(latest\)'),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            BlocBuilder<CounterBloc, int>(
              builder: (context, state) {
                return Text('$state',
                    style: TextStyle(color: Colors.blue, fontSize: 62));
              },
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                BlocBuilder<CounterBloc, int>(
                  builder: (context, state) {
                    final bloc = context.read<CounterBloc>();
                    return RaisedButton(
                      child: const Icon(Icons.undo),
                      onPressed: bloc.canUndo ? bloc.undo : null,
                      shape: StadiumBorder(),
                    );
                  },
                ),
                const SizedBox(width: 10),
                BlocBuilder<CounterBloc, int>(
                  builder: (context, state) {
                    final bloc = context.read<CounterBloc>();
                    return RaisedButton(
                      child: const Icon(Icons.redo),
                      onPressed: bloc.canRedo ? bloc.redo : null,
                      shape: StadiumBorder(),
                    );
                  },
                ),
                const SizedBox(height: 4),
              ],
            )
          ],
        ),
      ),
      floatingActionButton: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          FloatingActionButton(
            child: const Icon(Icons.add),
            onPressed: () => context.read<CounterBloc>().add(Increment()),
          ),
          const SizedBox(height: 4),
          FloatingActionButton(
            child: const Icon(Icons.remove),
            onPressed: () => context.read<CounterBloc>().add(Decrement()),
          ),
          const SizedBox(height: 4),

          // FloatingActionButton(
          //   child: const Icon(Icons.delete_forever),
          //   onPressed: () => context.read<CounterBloc>().add(Reset()),
          // ),
        ],
      ),
    );
  }
}

/// Base event class for the [CounterBloc].
class CounterEvent extends ReplayEvent {}

/// Notifies [CounterBloc] to increment its state.
class Increment extends CounterEvent {
  @override
  String toString() => 'Increment';
}

/// Notifies [CounterBloc] to decrement its state.
class Decrement extends CounterEvent {
  @override
  String toString() => 'Decrement';
}

/// Notifies [CounterBloc] to reset its state.
// class Reset extends CounterEvent {
//   @override
//   String toString() => 'Reset';
// }

/// {@template replay_counter_bloc}
/// A simple [ReplayBloc] which manages an `int` as its state
/// and reacts to three events: [Increment], [Decrement], and [Reset].
/// {@endtemplate}
class CounterBloc extends ReplayBloc<CounterEvent, int> {
  /// {@macro replay_counter_bloc}
  CounterBloc() : super(0) {
    on<Increment>((event, emit) => emit(state + 1));
    on<Decrement>((event, emit) => emit(state - 1));
    // on<Reset>((event, emit) => emit(0));
  }
}
