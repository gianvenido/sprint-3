//Gwneric Class
class MyData<T> {
  T? data;

  MyData(this.data);
}

class Pair<G, U, T> {
  G suami;
  U tahunMenikah;
  T istri;

  Pair(this.suami, this.istri, this.tahunMenikah);
}

//Generic Function
class ArrayHelper {
  static int count<T>(List<T> senarai) {
    return senarai.length;
  }
}

//Bounded Type Parameter
class NumberData<T extends num> {
  T data;

  NumberData(this.data);
}

//Dynamic
void printData(MyData data) {
  print(data.data);
}

//Type Checking
void checking(dynamic data) {
  if (data is MyData<String>) {
    print('Tipe data = String');
  } else if (data is MyData<num>) {
    print('Tipe data = Number');
  } else {
    print('Object');
  }
}

void main() {
  var nama = MyData<String>('Gian');
  var usia = MyData<int>(35);
  var isMarried = MyData(true);

  print('${nama.data}');
  print('${usia.data}');
  print('${isMarried.data}');

  var keluarga1 = Pair('Gian', 'Athaya', 2019);
  var keluarga2 = Pair('Burhan', false, 1995);

  //Generic Function
  var senarai1 = [1, 2, 74, 333, 24, 67, 8];
  var senarai2 = ['A', 'B', 'C', 'D'];

  print(ArrayHelper.count(senarai1));
  print(ArrayHelper.count(senarai2));

  //Bounded Type Parameter
  var nomor1 = NumberData(10);
  var nomor2 = NumberData(1.0);
  var nomor3 = NumberData(1030);

  //Typechecking

  checking(MyData('Gian'));
  checking(MyData(50));
  checking('true');
}
