import 'data/category.dart';

class Manager {
  String name = 'Arian';
  String company = 'Facebook';

  //Constructor
  Manager(this.name);

  String alliance() {
    return 'No Alliance';
  }

  void sayHello(tamu) => print(
      'Hello $tamu, my name is ${this.name}, as Manager. My Company is $company');
}

class ViceManager extends Manager {
  String? gender;
  String company = 'Alibaba';

  //Super Constructor
  ViceManager(String name) : super(name);

  String alliance() {
    return 'FedEX';
  }

  //method overriding : mendeklarasikan ulang method yang ada di parentnya
  void sayHello(tamu) => print(
      'Selamat datang $tamu, nama saya ${this.name}, sebagai ViceManager. My Company is $company');
}

class OtherManager extends ViceManager {
  // field overriding
  String name = 'Yahya';

  OtherManager(this.name) : super(name) {
    print('Create new other manager');
  }

  String alliance() {
    return 'Gojek';
  }

  // Super Keyword
  String getParentAlliance() {
    return super.alliance();
  }
}

//Abstract Class
abstract class Location {
  String? lokasi;

  // abstract method
  void getTransport();
}

class City extends Location {
  City(String daerah) {
    this.lokasi = daerah;
  }

  String getTransport() {
    return 'Transportation by plane';
  }
}

class Product {
  String? id;
  String? name;
  int? _quantity;

  int? getQuantity() {
    return _quantity;
  }

  //override toString method
  String toString() {
    return 'Product {id = $id, name = $name, quantity = $_quantity}';
  }
}

class Rectangle {
  int _width = 0;
  int _height = 0;

  // Getter
  int get lebar {
    return _height;
  }

  int get panjang {
    return _width;
  }

  // Setter
  set panjang(int value) {
    _width = value;
  }

  set lebar(int value) {
    _height = value;
  }
}

class Car {
  String name = '';

  void drive() {}

  int getTire() {
    return 0;
  }
}

abstract class hasUser {
  String getUser();
}

class Toyota implements Car, hasUser {
  String name = 'Avanza';
  String getUser() => 'Aldi';

  void drive() {
    print('$name is running');
  }

  int getTire() {
    return 4;
  }
}

abstract class Multimedia {}

mixin Playable on Multimedia {
  String? name;

  void play() {
    print('Play $name');
  }
}

mixin Stoppable on Multimedia {
  String? name;

  void stop() {
    print('Stop $name');
  }
}

class Video extends Multimedia with Playable, Stoppable {}

class Audio extends Multimedia with Playable, Stoppable {}

// Callable Class
class Sum {
  final int first;
  final int second;

  Sum(this.first, this.second);

  int call() {
    return first + second;
  }
}

typedef Total = Sum;
typedef Jumlah = Sum;

// Static Field
class Application {
  static final String book = 'The Legendary Saiyans';
  static final String author = 'Gian Pratama';
}

//Static Method
class Math {
  static int sum(int first, int second) => first + second;
}

//Enum
enum CustomerLevel { premium, regular, vip }

class Customer {
  String? name;
  CustomerLevel? level;

  Customer(this.name, this.level);
}

class ValidationException implements Exception {
  String message;

  ValidationException(this.message);
}

// Exception
class Validation {
  static void validate(String username, String password) {
    if (username == '') {
      throw ValidationException('User nggak ada bro');
    } else if (password == '') {
      throw ValidationException('Password nggak boleh kosong bro');
    } else if (username != 'Gian' || password != 'gian') {
      throw Exception('Login gagal bro.');
    }
    ;
    //valid
  }
}

// VOID

void katakanHalo(Manager person) {
  //Type Check & Casts
  if (person is Manager) {
    Manager man = person as Manager;
    print('Hello, Manager ${man.name}');
  } else if (person is ViceManager) {
    ViceManager vice = person as ViceManager;
    print('Hello, ViceManager ${vice.name}');
  } else {
    print('Hello, ${person.name}');
  }
}

void main() {
  //Exception
  //Try Catch
  try {
    Validation.validate('', 'giaan');
  } on ValidationException catch (exception, stackTrace) {
    print('Validation error: ${exception.message}');
    print('Stack Trace: ${stackTrace.toString()}');
  } on Exception catch (error, stackTrace) {
    print('Error: ${error.toString()}');
    print('Stack Trace: ${stackTrace.toString()}');
  } finally {
    print('Programm selesai');
  }

  //Callable class
  var sum = Sum(10, 15);
  var jumlah = Jumlah(10, 11);
  var total = Total(1, 26);

  //Static Field
  print(Application.book);
  print(Application.author);

  //Static Method
  print(Math.sum(10, 35));

  print(sum());
  print(jumlah());

  //Enum
  var consumer = Customer('Gian', CustomerLevel.premium);

  print(consumer.name);
  print(consumer.level);
  print(CustomerLevel.values);

  Manager person = Manager('Gunadi');
  print(person.name);
  katakanHalo(Manager('Galih'));

  person = ViceManager('Horus');
  print(person.name);
  katakanHalo(ViceManager('Gugus'));

  person = OtherManager('Alika');
  print(person.name);
  katakanHalo(Manager('Bujang'));

  //equals operator
  var category1 = Category('Gula', 'kg');
  var category2 = Category('Gula', 'kg');

  print(category1.name);
  print(category2.unit);

  print({category1 == category2});

  var kota = City('Poso');

  print(kota.lokasi);
  print(kota.getTransport());

  var product = Product();
  product.id = '1';
  product.name = 'diamond';
  product._quantity = 1;
  print(product);
}
