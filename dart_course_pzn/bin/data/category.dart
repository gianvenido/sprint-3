class Category {
  String? name;
  String? unit;

  Category(this.name, this.unit);

  //Override equals operator
  bool operator ==(Object other) {
    if (other is Category) {
      if (name != other.name) {
        return false;
      } else if (unit != other.unit) {
        return false;
      } else {
        return true;
      }
    } else {
      return false;
    }
  }

  //Override Hashcode
  int get hashCode {
    var result = name.hashCode;
    result += unit.hashCode;
    return result;
  }
}
