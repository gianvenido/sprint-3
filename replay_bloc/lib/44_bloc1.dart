import 'package:flutter/material.dart';
import '44_color_bloc.dart';

class Bloc1 extends StatefulWidget {
  //const Bloc1({ Key? key }) : super(key: key);

  @override
  State<Bloc1> createState() => _Bloc1State();
}

class _Bloc1State extends State<Bloc1> {
  ColorBloc bloc = ColorBloc();

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton:
          Row(mainAxisAlignment: MainAxisAlignment.end, children: [
        FloatingActionButton(
          onPressed: () {
            bloc.eventSink.add(ColorEvent.to_amber);
          },
          backgroundColor: Colors.amber,
        ),
        SizedBox(width: 10),
        FloatingActionButton(
          onPressed: () {
            bloc.eventSink.add(ColorEvent.to_blue);
          },
          backgroundColor: Colors.blue,
        ),
      ]),
      appBar: AppBar(title: Text('Video 44 - BloC tanpa Lib')),
      body: Center(
        child: StreamBuilder<Object>(
            stream: bloc.stateStream,
            initialData: Colors.red,
            builder: (context, snapshot) {
              return AnimatedContainer(
                width: 100,
                height: 100,
                duration: Duration(milliseconds: 350),
                color: snapshot.data,
              );
            }),
      ),
    );
  }
}
