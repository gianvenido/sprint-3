import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sbox/bloc/color_bloc.dart';

class Bloc45 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton:
          Row(mainAxisAlignment: MainAxisAlignment.end, children: [
        FloatingActionButton(
          onPressed: () {
            context.read<ColorBloc>().add(ToAmberEvent());
            print('Amber Click');
          },
          backgroundColor: Colors.amber,
        ),
        SizedBox(width: 10),
        FloatingActionButton(
          onPressed: () {
            context.read<ColorBloc>().add(ToPurpleEvent());
            print('Purple Click');
          },
          backgroundColor: Colors.purple,
        ),
        SizedBox(width: 10),
        FloatingActionButton(
          onPressed: () {
            context.read<ColorBloc>().add(ToGreenEvent());
            print('Green Click');
          },
          backgroundColor: Colors.green,
        ),
      ]),
      appBar: AppBar(title: Text('Video 45x - flutter_bloc (v8)')),
      body: Center(
        child: BlocBuilder<ColorBloc, ColorState>(
          builder: (context, state) {
            return AnimatedContainer(
              width: 100,
              height: 100,
              duration: Duration(milliseconds: 350),
              color: state.warna,
            );
          },
        ),
      ),
    );
  }
}
