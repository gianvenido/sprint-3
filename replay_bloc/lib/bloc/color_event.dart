part of 'color_bloc.dart';

@immutable
abstract class ColorEvent {}

class ToPurpleEvent extends ColorEvent {
  // final Color warna;

  // ToPurpleEvent({this.warna = Colors.purple});
}

class ToGreenEvent extends ColorEvent {}

class ToAmberEvent extends ColorEvent {}
