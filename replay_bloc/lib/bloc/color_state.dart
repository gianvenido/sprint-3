part of 'color_bloc.dart';

class ColorState {
  Color warna;

  ColorState(this.warna);
}

class ColorInitial extends ColorState {
  ColorInitial() : super(Colors.grey);
}

class ColorLoaded extends ColorState {
  ColorLoaded(Color warna) : super(warna);
}
