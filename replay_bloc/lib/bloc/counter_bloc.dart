import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'counter_event.dart';
part 'counter_state.dart';

class CounterBloc extends Bloc<CounterEvent, CounterState> {
  CounterBloc() : super(const CounterInitial(0)) {
    on<IncrementEvent>((event, emit) {
      emit(state is CounterLoaded
          ? CounterLoaded(state.angka + event.angka)
          : CounterLoaded(event.angka));
    });

    on<DecrementEvent>((event, emit) {
      emit(state is CounterLoaded
          ? CounterLoaded(state.angka - 1)
          : CounterLoaded(-1));
    });
  }
}
