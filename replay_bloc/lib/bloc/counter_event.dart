part of 'counter_bloc.dart';

@immutable
abstract class CounterEvent {}

class IncrementEvent extends CounterEvent {
  final int angka;

  IncrementEvent({this.angka = 1});
}

class DecrementEvent extends CounterEvent {}
