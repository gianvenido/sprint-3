part of 'counter_bloc.dart';

@immutable
class CounterState {
  final int angka;

  const CounterState(this.angka);
}

class CounterInitial extends CounterState {
  const CounterInitial(int angka) : super(0);
}

class CounterLoaded extends CounterState {
  const CounterLoaded(int angka) : super(angka);
}
