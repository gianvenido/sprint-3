import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

enum ColorEvent { toAmber, toPurple, toBlue }

// class ColorBloc extends Bloc<ColorEvent, Color> {
//   ColorBloc() : super(Colors.grey);

//   @override
//   Color get initialState => Colors.amber;

//   @override
//   Stream<Color> mapToEventState(ColorEvent event) async* {
//     yield (event == ColorEvent.toAmber
//         ? Colors.amber
//         : (event == ColorEvent.toPurple ? Colors.purple : Colors.lightBlue));
//   }
// }

class ColorBloc extends Bloc<ColorEvent, Color> {
  ColorBloc() : super(Colors.grey) {
    on<ColorEvent>((event, emit) {
      (event == ColorEvent.toAmber
          ? emit(Colors.amber)
          : (event == ColorEvent.toPurple
              ? emit(Colors.purple)
              : emit(Colors.lightBlue)));
    });
  }
}
