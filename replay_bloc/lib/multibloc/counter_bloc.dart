import 'package:bloc/bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// class CounterBloc extends Bloc<int, int> {
//   CounterBloc() : super(0);

//   @override
//   int get initialState => 0;

//   @override
//   Stream<int> mapEventToState(int event) async* {
//     yield event;
//   }
// }

class CounterBloc extends Bloc<int, int> {
  CounterBloc() : super(0) {
    on<int>((event, emit) {
      emit(event);
    });
  }
}
