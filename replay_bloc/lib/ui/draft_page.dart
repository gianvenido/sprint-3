import 'package:flutter/material.dart';

class DraftPage extends StatelessWidget {
  // const DraftPage({ Key? key }) : super(key: key);

  final Color backgroundColor;
  final Widget body;

  DraftPage({this.body, this.backgroundColor = Colors.amber});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Video 54x - MultiBloC (v8.0.1)'),
        backgroundColor: backgroundColor,
      ),
      body: body,
    );
  }
}
