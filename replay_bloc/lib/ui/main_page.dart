import 'package:sbox/multibloc/color_bloc.dart';
import 'package:sbox/multibloc/counter_bloc.dart';
import 'package:sbox/ui/second_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'draft_page.dart';

class MainPage extends StatelessWidget {
  // const MainPage({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ColorBloc, Color>(
      builder: (context, color) => DraftPage(
          backgroundColor: color,
          body: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                BlocBuilder<CounterBloc, int>(
                  builder: (context, number) => Text('$number',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 48)),
                ),
                RaisedButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => SecondPage()));
                    },
                    child: Text('Click to change',
                        style: TextStyle(color: Colors.white)),
                    color: color,
                    shape: StadiumBorder()),
              ],
            ),
          )),
    );
  }
}
