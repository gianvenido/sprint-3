import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sbox/multibloc/color_bloc.dart';
import 'package:sbox/multibloc/counter_bloc.dart';
import 'draft_page.dart';

class SecondPage extends StatelessWidget {
  // const SecondPage({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ColorBloc colorBloc = BlocProvider.of<ColorBloc>(context);
    CounterBloc counterBloc = BlocProvider.of<CounterBloc>(context);

    return BlocBuilder<ColorBloc, Color>(
      builder: (context, color) {
        return DraftPage(
          backgroundColor: color,
          body: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text('\(Tap number or circle to change\)'),
                SizedBox(height: 40),
                BlocBuilder<CounterBloc, int>(
                  builder: (context, number) {
                    return GestureDetector(
                      onTap: () {
                        counterBloc.add(number + 1);
                      },
                      child: Text('$number',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 48)),
                    );
                  },
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    RaisedButton(
                      onPressed: () {
                        colorBloc.add(ColorEvent.toAmber);
                      },
                      color: Colors.amber,
                      shape: (color == Colors.amber)
                          ? CircleBorder(
                              side: BorderSide(color: Colors.black26, width: 3),
                            )
                          : CircleBorder(),
                    ),
                    RaisedButton(
                      onPressed: () {
                        colorBloc.add(ColorEvent.toPurple);
                      },
                      color: Colors.purple,
                      shape: (color == Colors.purple)
                          ? CircleBorder(
                              side: BorderSide(color: Colors.black26, width: 3),
                            )
                          : CircleBorder(),
                    ),
                    RaisedButton(
                      onPressed: () {
                        colorBloc.add(ColorEvent.toBlue);
                      },
                      color: Colors.lightBlue,
                      shape: (color == Colors.lightBlue)
                          ? CircleBorder(
                              side: BorderSide(color: Colors.black26, width: 3),
                            )
                          : CircleBorder(),
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
